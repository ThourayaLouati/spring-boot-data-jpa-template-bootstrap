package tn.esprit.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entity.Employe;
import tn.esprit.spring.repository.EmployeRepository;


@Service

public class EmployeServiceImpl implements IEmployeService
{   
	
	@Autowired
	EmployeRepository employeRepository;

	
	public Employe getEmployeByEmailAndPassword(String login, String password) {
		
		return employeRepository.getEmployeByEmailAndPassword(login, password);
	}


	

}
